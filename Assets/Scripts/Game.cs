﻿using UnityEngine;
using System.IO;
using System.Collections;
using System.Collections.Generic;

using TiledSharp;

public class Game : MonoBehaviour
{
	public CameraFollower cam;

	private struct LocationInfo
	{
		public string name;
		public int mapX;
		public int mapY;
		public int startX;
		public int startY;
	}

	private List<LocationInfo> locationInfos;

	private class TileSetInfo
	{
		public TmxTileset tileset;
		public Texture2D texture;
		public Material material;
		public int numX;
		public int numY;
		public int offsetX;
		public int offsetY;
		public int startX;
		public int startY;
		public int firstGid;
		public int tileWidth;
		public int tileHeight;
		public int texWidth;
		public int texHeight;
		public bool hasAlpha;
	}

	public UnityEngine.UI.Dropdown locationDropdown;

	public Character character;

	private TmxMap map;
	private TileSetInfo[] tileSetInfos;

	private byte[] collisionMap;

	private List<int> tileSetMap;

	private List<Vector3> vertices;
	private List<Vector2> uvs;

	private List<int>[] triangles;

	private Dictionary<int, TmxTilesetTile> animatedTiles;

	private List<Mesh> meshes;

	private float width;
	private float height;

	private float tileWidth;
	private float tileHeight;

	private int mapWidth;
	private int mapHeight;

	private int mapXOffset;
	private int mapYOffset;

	private int numTilesets;

	private int maxRows;
	private int maxCols;

	private int numCells;

	private int numLayers;
	private int numBelowLayers;

	private uint gameDuration; //millisecond

	private int[,] meshMap;

	private LinkedList<TmxLayerTile>[] visibleTileList;

	private int visibleCol0;
	private int visibleCol1;
	
	private int visibleRow0;
	private int visibleRow1;

	private bool isNewMap;

	[HideInInspector]
	public int mapX = 6;

	[HideInInspector]
	public int mapY = 0;

	private int curLocationId;
	private int nextLocationId;

	private Dictionary<KeyValuePair<int, int>, bool> mapValidityCache;

	void Awake()
	{
		tileSetMap = new List<int>();

		vertices = new List<Vector3>();
		uvs = new List<Vector2>();

		meshes = new List<Mesh>();

		animatedTiles = new Dictionary<int, TmxTilesetTile>();
		locationInfos = new List<LocationInfo>();

		mapValidityCache = new Dictionary<KeyValuePair<int, int>, bool>();

		height = Camera.main.orthographicSize * 2.0f;
		width = height * Camera.main.aspect;

		character.game = this;
	}

	bool LoadMap(int x, int y)
	{
		bool hasCache = false;

		KeyValuePair<int, int> key = new KeyValuePair<int, int>(x, y);

		bool isValid;
		if (mapValidityCache.TryGetValue(key, out isValid))
		{
			if (!isValid) return false;
			hasCache = true;
		}

		string filepath = "Maps/" + x.ToString() + "." + y.ToString() + ".tmx"; 
		TmxMap newMap = new TmxMap(filepath);

		if (newMap.HasError) 
		{
			if (!hasCache) mapValidityCache.Add(key, false);
			else mapValidityCache[key] = false;

			return false;
		}

		map = newMap;
		newMap = null;

		animatedTiles.Clear();

		isNewMap = true;

		Debug.Log("Load Map : " + x.ToString() + ", " + y.ToString());
		
		mapXOffset = int.Parse(map.Properties["xOffsetModifier"]);
		mapYOffset = int.Parse(map.Properties["yOffsetModifier"]);

		tileWidth = map.TileWidth;
		tileHeight = map.TileHeight;
		
		mapWidth = map.Width;
		mapHeight = map.Height;

		numCells = mapWidth * mapHeight;
				
		cam.minX = width / 2.0f;
		cam.maxX = mapWidth * tileWidth - width / 2.0f;
		
		if (cam.minX > cam.maxX) cam.minX = cam.maxX = mapWidth * tileWidth / 2.0f;
		
		cam.minY = -mapHeight * tileHeight + height / 2.0f;
		cam.maxY = -height / 2.0f;
		
		if (cam.minY > cam.maxY) cam.minY = cam.maxY = -mapHeight * tileHeight / 2.0f;
						
		maxRows = Mathf.CeilToInt(height / tileHeight - 1e-5f);
		maxCols = Mathf.CeilToInt(width / tileWidth - 1e-5f);
		
		numLayers = map.Layers.Count;
		
		gameDuration = 0;

		visibleTileList = new LinkedList<TmxLayerTile>[numLayers];
		for (int i = 0; i < numLayers; ++i)
		{
			visibleTileList[i] = new LinkedList<TmxLayerTile>();
		}
			
		numTilesets = map.Tilesets.Count;
		tileSetInfos = new TileSetInfo[numTilesets];

		int oldNumLayers = meshes.Count;

		for (int i = 0; i < oldNumLayers; ++i)
		{
			meshes[i].Clear();
		}
		
		if (oldNumLayers < numLayers)
		{
			for (int i = oldNumLayers; i < numLayers; ++i)
			{
				Mesh mesh = new Mesh(); 
				mesh.MarkDynamic();
				
				meshes.Add(mesh);	
			}
		}
		else if (oldNumLayers > numLayers)
		{
			for (int i = numLayers; i < oldNumLayers; ++i)
			{
				GameObject.Destroy(meshes[i]);
			}

			meshes.RemoveRange(numLayers, oldNumLayers - numLayers);
		}

		tileSetMap.Clear();
		int numTiles = 0;
		
		Shader opaqueShader = Shader.Find("Custom/UnlitTexture");
		Shader transparentShader = Shader.Find("Custom/UnlitTransparent");
		
		//Shader opaqueShader = Shader.Find("Unlit/Color");
		//Shader transparentShader = Shader.Find("Unlit/Transparent");
		
		numBelowLayers = 0;
		for (int i = 0; i < numLayers; ++i)
		{
			if (!map.Layers[i].Name.Equals("WalkBehind")) numBelowLayers++;
		}
		
		triangles = new List<int>[numTilesets];
		meshMap = new int[numLayers, numTilesets];
		
		for (int i = 0; i < numTilesets; ++i)
		{
			triangles[i] = new List<int>();
			
			TmxTileset ts = map.Tilesets[i];
			
			TileSetInfo tileSetInfo = new TileSetInfo();
			tileSetInfos[i] = tileSetInfo;
			
			tileSetInfo.tileset = ts;
			
			string imagePath = ts.Image.Source;
			System.Uri uri = new System.Uri("file:///" + imagePath);
			string dir = Path.GetDirectoryName(uri.AbsolutePath.Substring(1));
			
			Texture2D texture = Utility.GetTexture(dir + "/" + Path.GetFileNameWithoutExtension(imagePath));
			tileSetInfo.texture = texture;
			//tileSetInfo.hasAlpha = texture.alphaIsTransparency;
			tileSetInfo.hasAlpha = true;
			
			Shader shader = tileSetInfo.hasAlpha ? transparentShader : opaqueShader;
			Material material = new Material(shader);
			
			tileSetInfo.material = material;
			tileSetInfo.material.mainTexture = texture;
			
			var wStart = ts.Margin;
			var wInc = ts.TileWidth + ts.Spacing;
			var wEnd = tileSetInfo.texture.width;
			
			var hStart = ts.Margin;
			var hInc = ts.TileHeight + ts.Spacing;
			var hEnd = tileSetInfo.texture.height;
			
			tileSetInfo.texWidth = wEnd;
			tileSetInfo.texHeight = hEnd;
			
			tileSetInfo.tileWidth = ts.TileWidth;
			tileSetInfo.tileHeight = ts.TileHeight;
			
			tileSetInfo.numX = (wEnd - wStart) / wInc;
			tileSetInfo.numY = (hEnd - hStart) / hInc;
			
			tileSetInfo.startX = wStart;
			tileSetInfo.startY = hStart;
			
			tileSetInfo.offsetX = wInc;
			tileSetInfo.offsetY = hInc;
			
			tileSetInfo.firstGid = ts.FirstGid;
			
			var id = tileSetInfo.firstGid;
			
			while (numTiles < id) 
			{
				tileSetMap.Add(-1);
				numTiles++;
			}
			
			int numTilesetTiles = tileSetInfo.numX * tileSetInfo.numY;
			for (int j = 0; j < numTilesetTiles; ++j)
			{
				tileSetMap.Add(i);
				numTiles++;
			}
			
			foreach (TmxTilesetTile tile in ts.Tiles)
			{
				if (tile.AnimationFrames.Count > 0)
				{
					animatedTiles.Add(tile.Id + ts.FirstGid, tile);
				}
			}
			
			//Debug.Log (id + " " + numTilesetTiles + " " + texture.name);
		}
		
		collisionMap = new byte[mapWidth * mapHeight];
		
		for (int i = 0; i < mapHeight; ++i)
		{	
			for (int j = 0; j < mapWidth; ++j)
			{
				int id = i * mapWidth + j;
				collisionMap[id] = 0;
			}
		}

		for (int k = 0; k < numLayers; ++k)
		{
			TmxLayer layer = map.Layers[k];

			for (int i = 0; i < numCells; ++i)
			{
				TmxLayerTile tile = layer.Tiles[i];
				int gid = tile.Gid;
				if (gid == 0) continue;
				
				int tileSetId = tileSetMap[gid];
				if (tileSetId == -1) continue;

				tile.tileSetId = tileSetId;

				int tileX = tile.X;
				int tileY = tile.Y;

				TileSetInfo tileSetInfo = tileSetInfos[tileSetId];

				TmxTilesetTile animatedTile;
				if (animatedTiles.TryGetValue(tile.Gid, out animatedTile))
				{
					tile.animatedTile = animatedTile;

					if (animatedTile.uvs == null)
					{
						int numFrames = animatedTile.AnimationFrames.Count;
						Vector2[] tileUvs = new Vector2[numFrames * 4];
						animatedTile.uvs = tileUvs;

						int pos = 0;
						for (int j = 0; j < numFrames; ++j)
						{
							int tileId = animatedTile.AnimationFrames[j].Id;

							int tx = tileId % tileSetInfo.numX;
							int ty = tileId / tileSetInfo.numX;
							
							float x0 = tileSetInfo.startX + tx * tileSetInfo.offsetX + 0.5f;
							float y0 = tileSetInfo.texHeight - (tileSetInfo.startY + ty * tileSetInfo.offsetY) - tileSetInfo.tileHeight + 0.5f;
							
							float x1 = x0 + tileSetInfo.tileWidth - 1.0f;
							float y1 = y0 + tileSetInfo.tileHeight - 1.0f;
							
							x0 /= tileSetInfo.texWidth;
							x1 /= tileSetInfo.texWidth;
							
							y0 /= tileSetInfo.texHeight;
							y1 /= tileSetInfo.texHeight;
							
							tileUvs[pos++] = new Vector2(x0, y0);
							tileUvs[pos++] = new Vector2(x1, y0);
							tileUvs[pos++] = new Vector2(x1, y1);
							tileUvs[pos++] = new Vector2(x0, y1);
						}				
					}
				}
				else 
				{
					int tileId = tile.Gid - tileSetInfo.firstGid;

					int tx = tileId % tileSetInfo.numX;
					int ty = tileId / tileSetInfo.numX;
					
					float x0 = tileSetInfo.startX + tx * tileSetInfo.offsetX + 0.5f;
					float y0 = tileSetInfo.texHeight - (tileSetInfo.startY + ty * tileSetInfo.offsetY) - tileSetInfo.tileHeight + 0.5f;
					
					float x1 = x0 + tileSetInfo.tileWidth - 1.0f;
					float y1 = y0 + tileSetInfo.tileHeight - 1.0f;
					
					x0 /= tileSetInfo.texWidth;
					x1 /= tileSetInfo.texWidth;
					
					y0 /= tileSetInfo.texHeight;
					y1 /= tileSetInfo.texHeight;
					
					Vector2[] tileUvs = new Vector2[4];
					tile.uvs = tileUvs;
					tileUvs[0] = new Vector2(x0, y0);
					tileUvs[1] = new Vector2(x1, y0);
					tileUvs[2] = new Vector2(x1, y1);
					tileUvs[3] = new Vector2(x0, y1);
				}
				
				float z = (k < numBelowLayers ? (numBelowLayers - k) : -(k - numBelowLayers + 1));
				
				Vector3[] tileVertices = new Vector3[4];
				tile.vertices = tileVertices;
				tileVertices[0] = new Vector3(tileX * tileWidth, -tileY * tileHeight - tileHeight, z);
				tileVertices[1] = new Vector3(tileX * tileWidth + tileSetInfo.tileWidth, -tileY * tileHeight - tileHeight, z);
				tileVertices[2] = new Vector3(tileX * tileWidth + tileSetInfo.tileWidth, -tileY * tileHeight + tileSetInfo.tileHeight - tileHeight, z);
				tileVertices[3] = new Vector3(tileX * tileWidth, -tileY * tileHeight + tileSetInfo.tileHeight - tileHeight, z);
			}
			
			byte mask = 0;
			if (layer.Name.Equals("Collisions") || layer.Name.Equals("Water")) mask = 15;
			else if (layer.Name.Equals("LedgesLeft")) mask = 1;
			else if (layer.Name.Equals("LedgesUp")) mask = 2;
			else if (layer.Name.Equals("LedgesRight")) mask = 4;
			else if (layer.Name.Equals("LedgesDown")) mask = 8;
			else continue;
	
			for (int i = 0; i < numCells; ++i)
			{
				TmxLayerTile tile = layer.Tiles[i];
				int gid = tile.Gid;
				
				int tileSetId = tileSetMap[gid];
				if (tileSetId == -1) continue;
				
				collisionMap[i] |= mask;
			}
		}

		System.GC.Collect();
		Resources.UnloadUnusedAssets();

		if (!hasCache) mapValidityCache.Add(key, true);
		else mapValidityCache[key] = true;

		return true;
	}

	public void OnSelectLocation()
	{
		nextLocationId = locationDropdown.value;
	}

	// Use this for initialization
	void Start () 
	{
		TextAsset locationData = Resources.Load<TextAsset>("location");
		string[] lines = locationData.text.Split(new char[] {'\r', '\n' }, System.StringSplitOptions.RemoveEmptyEntries);

		int pos = 0;
		while (pos < lines.Length)
		{
			string locationName = lines[pos++].Trim();
			if (locationName.Length == 0) break;

			string[] words = lines[pos++].Split();
			LocationInfo locationInfo = new LocationInfo();
			locationInfo.name = locationName;
			locationInfo.mapX = int.Parse(words[0]);
			locationInfo.mapY = int.Parse(words[1]);
			locationInfo.startX = int.Parse(words[2]);
			locationInfo.startY = int.Parse(words[3]);

			locationInfos.Add(locationInfo);
		}
		
		List<UnityEngine.UI.Dropdown.OptionData> optionList = new List<UnityEngine.UI.Dropdown.OptionData>();
		for (int i = 0; i < locationInfos.Count; ++i)
		{
			UnityEngine.UI.Dropdown.OptionData optionData = new UnityEngine.UI.Dropdown.OptionData(locationInfos[i].name);
			optionList.Add(optionData);
		}
		
		locationDropdown.options = optionList;
		curLocationId = -1;

		OnSelectLocation();

		cam.obj = character.transform;
	}

	public void GetTileCoordFromPosition(float x, float y, out int tx, out int ty)
	{
		tx = (int)(x / tileWidth);
		ty = (int)(-y / tileHeight);
	}

	public void GetTileRect(int tx, int ty, out float x0, out float y0, out float x1, out float y1)
	{
		x0 = tx * tileWidth;
		x1 = x0 + tileWidth;

		y1 = -ty * tileHeight;
		y0 = y1 - tileHeight;
	}

	public bool IsTileMoveable(int tx, int ty, int dir)
	{
		if (IsTileOutOfBound(tx, ty)) return false;

		int id = ty * mapWidth + tx;
		return (collisionMap[id] & (1 << dir)) == 0;
	}

	public bool IsTileOutOfBound(int tx, int ty)
	{
		if (tx < 0 || tx >= mapWidth) return true;
		if (ty < 0 || ty >= mapHeight) return true;

		return false;
	}

	public void AddVisibleTile(int layerId, TmxLayerTile tile)
	{
		if (tile.visibleListNode != null) return;
		tile.visibleListNode = visibleTileList[layerId].AddLast(tile);
	}

	public void RemoveVisibleTile(int layerId, TmxLayerTile tile)
	{
		if (tile.visibleListNode == null) return;
		visibleTileList[layerId].Remove(tile.visibleListNode);
		tile.visibleListNode = null;
	}

	void UpdateMesh()
	{
		Vector3 cameraPos = cam.transform.position;
		float camX = cameraPos.x;
		float camY = cameraPos.y;
		
		float left = camX - width / 2.0f;
		float right = camX + width / 2.0f;
		
		float down = camY - height / 2.0f;
		float up = camY + height / 2.0f;
		
		int col0 = Mathf.Clamp(Mathf.FloorToInt(left / tileWidth), 0, mapWidth - 1);
		int col1 = Mathf.Clamp(Mathf.FloorToInt(right / tileWidth), 0, mapWidth - 1);
		
		int row0 = Mathf.Clamp(Mathf.FloorToInt(-up / tileHeight), 0, mapHeight - 1);
		int row1 = Mathf.Clamp(Mathf.FloorToInt(-down / tileHeight), 0, mapHeight - 1);

		for (int k = 0; k < numLayers; ++k)
		{
			TmxLayer layer = map.Layers[k];

			if (isNewMap)
			{
				for (int j = col0; j <= col1; ++j)
				{
					for (int i = row0; i <= row1; ++i)
					{
						int id = i * mapWidth + j;
						
						TmxLayerTile tile = layer.Tiles[id];
						if (tile.vertices != null) AddVisibleTile(k, tile);
					}
				}
			}
			else if (col0 > visibleCol1 || col1 < visibleCol0 || row0 > visibleRow1 || row1 < visibleRow0)
			{
				for (int j = visibleCol0; j <= visibleCol1; ++j)
				{
					for (int i = visibleRow0; i <= visibleRow1; ++i)
					{
						int id = i * mapWidth + j;
						
						TmxLayerTile tile = layer.Tiles[id];
						if (tile.vertices != null) RemoveVisibleTile(k, tile);
					}
				}

				for (int j = col0; j <= col1; ++j)
				{
					for (int i = row0; i <= row1; ++i)
					{
						int id = i * mapWidth + j;
						
						TmxLayerTile tile = layer.Tiles[id];
						if (tile.vertices != null) AddVisibleTile(k, tile);
					}
				}
			}
			else 
			{
				int iCol0 = Mathf.Max(visibleCol0, col0);
				int iCol1 = Mathf.Min(visibleCol1, col1);
				int iRow0 = Mathf.Max(visibleRow0, row0);
				int iRow1 = Mathf.Min(visibleRow1, row1);

				for (int j = visibleCol0; j < iCol0; ++j)
				{
					for (int i = visibleRow0; i <= visibleRow1; ++i)
					{
						int id = i * mapWidth + j;
						
						TmxLayerTile tile = layer.Tiles[id];
						if (tile.vertices != null) RemoveVisibleTile(k, tile);
					}
				}

				for (int j = iCol1 + 1; j <= visibleCol1; ++j)
				{
					for (int i = visibleRow0; i <= visibleRow1; ++i)
					{
						int id = i * mapWidth + j;
						
						TmxLayerTile tile = layer.Tiles[id];
						if (tile.vertices != null) RemoveVisibleTile(k, tile);
					}
				}

				for (int i = visibleRow0; i < iRow0; ++i)
				{
					for (int j = iCol0; j <= iCol1; ++j)
					{
						int id = i * mapWidth + j;
						
						TmxLayerTile tile = layer.Tiles[id];
						if (tile.vertices != null) RemoveVisibleTile(k, tile);
					}
				}

				for (int i = iRow1 + 1; i <= visibleRow1; ++i)
				{
					for (int j = iCol0; j <= iCol1; ++j)
					{
						int id = i * mapWidth + j;
						
						TmxLayerTile tile = layer.Tiles[id];
						if (tile.vertices != null) RemoveVisibleTile(k, tile);
					}
				}

				for (int j = col0; j < iCol0; ++j)
				{
					for (int i = row0; i <= row1; ++i)
					{
						int id = i * mapWidth + j;
						
						TmxLayerTile tile = layer.Tiles[id];
						if (tile.vertices != null) AddVisibleTile(k, tile);
					}
				}
				
				for (int j = iCol1 + 1; j <= col1; ++j)
				{
					for (int i = row0; i <= row1; ++i)
					{
						int id = i * mapWidth + j;
						
						TmxLayerTile tile = layer.Tiles[id];
						if (tile.vertices != null) AddVisibleTile(k, tile);
					}
				}
				
				for (int i = row0; i < iRow0; ++i)
				{
					for (int j = iCol0; j <= iCol1; ++j)
					{
						int id = i * mapWidth + j;
						
						TmxLayerTile tile = layer.Tiles[id];
						if (tile.vertices != null) AddVisibleTile(k, tile);
					}
				}
				
				for (int i = iRow1 + 1; i <= row1; ++i)
				{
					for (int j = iCol0; j <= iCol1; ++j)
					{
						int id = i * mapWidth + j;
						
						TmxLayerTile tile = layer.Tiles[id];
						if (tile.vertices != null) AddVisibleTile(k, tile);
					}
				}
			}
		}

		isNewMap = false;
		
		visibleRow0 = row0;
		visibleRow1 = row1;
		visibleCol0 = col0;
		visibleCol1 = col1;

		for (int k = 0; k < numLayers; ++k)
		{
			for (int i = 0; i < numTilesets; ++i)
			{
				triangles[i].Clear();
			}
			
			int numVertices = 0;
			
			TmxLayer layer = map.Layers[k];
			
			Mesh mesh = meshes[k];
			mesh.Clear();
			
			vertices.Clear();
			uvs.Clear();

			for (LinkedListNode<TmxLayerTile> node = visibleTileList[k].First; node != null; node = node.Next)
			{
				TmxLayerTile tile = node.Value;
	
				if (tile.animatedTile != null)
				{
					TmxTilesetTile animatedTile = tile.animatedTile;
					int animationFrameId = 0;
					
					int totalDuration = animatedTile.AnimationDuration;
					
					int numAnimationFrames = animatedTile.AnimationFrames.Count;
					int timeOffset = (int)(gameDuration % (uint)totalDuration);
					
					for (int v = 0; v < numAnimationFrames; ++v)
					{
						TmxAnimationFrame frame = animatedTile.AnimationFrames[v];
						if (timeOffset < frame.Duration) 
						{
							animationFrameId = frame.Id;
							break;
						}
						else timeOffset -= frame.Duration;
					}
					
					Vector2[] tileUvs = animatedTile.uvs;
					
					int pos = animationFrameId * 4;
					uvs.Add(tileUvs[pos++]);
					uvs.Add(tileUvs[pos++]);
					uvs.Add(tileUvs[pos++]);
					uvs.Add(tileUvs[pos]);
				}
				else 
				{
					Vector2[] tileUvs = tile.uvs;
					
					uvs.Add(tileUvs[0]);
					uvs.Add(tileUvs[1]);
					uvs.Add(tileUvs[2]);
					uvs.Add(tileUvs[3]);
				}

				Vector3[] tileVertices = tile.vertices;
								
				vertices.Add(tileVertices[0]);
				vertices.Add(tileVertices[1]);
				vertices.Add(tileVertices[2]);
				vertices.Add(tileVertices[3]);
				
				List<int> triangleList = triangles[tile.tileSetId];
				
				triangleList.Add(numVertices);
				triangleList.Add(numVertices + 2);
				triangleList.Add(numVertices + 1);
				triangleList.Add(numVertices);
				triangleList.Add(numVertices + 3);
				triangleList.Add(numVertices + 2);
				
				numVertices += 4;
			}

			int numSubmeshes = 0;
			
			for (int i = 0; i < numTilesets; ++i)
			{
				if (triangles[i].Count > 0)
				{
					meshMap[k, numSubmeshes++] = i;
				}
			}

			mesh.subMeshCount = numSubmeshes;
			
			mesh.SetVertices(vertices);
			mesh.SetUVs(0, uvs);
							
			for (int i = 0; i < numSubmeshes; ++i)
			{
				mesh.SetTriangles(triangles[meshMap[k, i]], i);
			}
		}
	}

	void DrawMesh()
	{
		for (int k = 0; k < numLayers; ++k)
		{
			Mesh mesh = meshes[k];
			int numSubMeshes = mesh.subMeshCount;
			
			for (int i = 0; i < numSubMeshes; ++i)
			{
				Material mat = tileSetInfos[meshMap[k, i]].material;
				Graphics.DrawMesh(mesh, Vector3.zero, Quaternion.identity, mat, 0, null, i, null, false, false);
			}
		}
	}

	// Update is called once per frame
	void Update() 
	{
		float dt = Time.deltaTime;
		gameDuration += (uint)(dt * 100.0f);

		if (nextLocationId != curLocationId)
		{
			curLocationId = nextLocationId;
			LocationInfo locationInfo = locationInfos[nextLocationId];

			mapX = locationInfo.mapX;
			mapY = locationInfo.mapY;

			LoadMap(mapX, mapY);

			character.SetPosition(locationInfo.startX / 2.0f + tileWidth / 2.0f, -locationInfo.startY / 2.0f - tileHeight);

			cam.ForcePosition();
		}

		character.update(dt);
		int nextMapDir = character.nextMapDir;
		if (nextMapDir >= 0)
		{
			character.nextMapDir = -1;

			int oldMapX = mapX;			
			int oldMapY = mapY;			

			if (nextMapDir == 2)
			{
				mapX -= 1;
			}
			else if (nextMapDir == 0)
			{
				mapX += 1;
			}
			else if (nextMapDir == 3)
			{
	 			mapY -= 1;
			}
			else
			{
				mapY += 1;
			}

			int oldMapXOffset = mapXOffset;
			int oldMapYOffset = mapYOffset;

			if (LoadMap(mapX, mapY))
			{
				if (nextMapDir == 2)
				{
					character.SetPosition(mapWidth * tileWidth - character.width / 2.0f, character.y - (oldMapYOffset - mapYOffset) / 2.0f);
				}
				else if (nextMapDir == 0)
				{
					character.SetPosition(character.width / 2.0f, character.y - (oldMapYOffset - mapYOffset) / 2.0f);
				}
				else if (nextMapDir == 3)
				{
					character.SetPosition(character.x + (oldMapXOffset - mapXOffset) / 2.0f, -mapHeight * tileHeight);
				}
				else
				{
					character.SetPosition(character.x + (oldMapXOffset - mapXOffset) / 2.0f, -character.height);
				}

				cam.ForcePosition();
			}
			else 
			{
				mapX = oldMapX;
				mapY = oldMapY;
			}
		}

		UpdateMesh();
		DrawMesh();
	}
}
