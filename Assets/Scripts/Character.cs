using UnityEngine;
using System.Collections;

public class Character : MonoBehaviour
{
	public float speed;
	public float width;
	public float height;

	[HideInInspector]
	public Game game;

	private float x0;
	private float x1;

	private float y0;
	private float y1;

	[HideInInspector]
	public float x;

	[HideInInspector]
	public float y;

	private Animator animator;

	private int dir;
	private bool isWalking;

	private int curAnimationId;
	private readonly string[] directionNames = new string[]{"right", "down", "left", "up"};

	[HideInInspector]
	public int nextMapDir;

	private int keyMask = 0;

	void Awake()
	{
		animator = GetComponent<Animator>();
	}

	// Use this for initialization
	void Start()
	{
		Vector3 pos = transform.position;
		x = pos.x;
		y = pos.y;

		CalculateBound();

		dir = 0;
		isWalking = false;

		curAnimationId = -1;

		nextMapDir = -1;

		keyMask = 0;
	}

	private void CalculateBound()
	{
		x0 = x - width / 2.0f;
		x1 = x + width / 2.0f;

		y0 = y;
		y1 = y + height;
	}

	private void UpdatePosition()
	{
		Vector3 pos = transform.position;
		pos.x = x;
		pos.y = y;
		transform.position = pos;
	}

	public void SetPosition(float x, float y)
	{
		this.x = x;
		this.y = y;

		nextMapDir = -1;

		CalculateBound();

		UpdatePosition();
	}

	public void PressKey(int id)
	{
		keyMask |= (1 << id);
	}

	public void UnpressKey(int id)
	{
		keyMask &= ~(1 << id);
	}

	public bool IsPressingKey(int id)
	{
		return (keyMask & (1 << id)) != 0;
	}
	
	public void update(float dt)
	{
		int dx = 0;
		int dy = 0;

	#if UNITY_EDITOR
		if (Input.GetKey(KeyCode.LeftArrow) || IsPressingKey(2))
	#else
		if (IsPressingKey(2)) 
	#endif
		{
			dx -= 1;
		}
		
	#if UNITY_EDITOR
		if (Input.GetKey(KeyCode.RightArrow) || IsPressingKey(3))
	#else
		if (IsPressingKey(3)) 
	#endif
		{
			dx += 1;
		}

	#if UNITY_EDITOR
		if (Input.GetKey(KeyCode.UpArrow) || IsPressingKey(0))
	#else
		if (IsPressingKey(0)) 
	#endif
		{
			dy += 1;
		}
		
	#if UNITY_EDITOR
		if (Input.GetKey(KeyCode.DownArrow) || IsPressingKey(1))
	#else
		if (IsPressingKey(1)) 
	#endif
		{
			dy -= 1;
		}

		if (dx != 0)
		{
			if (dx < 0) dir = 2;
			else dir = 0;
		}
		else if (dy != 0)
		{
			if (dy < 0) dir = 1;
			else dir = 3;
		}

		if (dx != 0 || dy != 0)
		{
			isWalking = true;

			float move = speed * dt / Mathf.Sqrt(dx * dx + dy * dy);

			int tx, ty;

			float moveX = move * dx;
			float moveY = move * dy;

			float tx0, ty0, tx1, ty1;

			if (dx != 0)
			{
				if (dx < 0)
				{
					game.GetTileCoordFromPosition(x0 + 1e-3f, y0 + 1e-3f, out tx, out ty);
					if (game.IsTileOutOfBound(tx - 1, ty) && nextMapDir == -1) nextMapDir = 2;

					if (!game.IsTileMoveable(tx - 1, ty, 2))
					{
						game.GetTileRect(tx, ty, out tx0, out ty0, out tx1, out ty1);
						moveX = Mathf.Max(moveX, tx0 - x0);
					}

					game.GetTileCoordFromPosition(x0 + 1e-3f, y1 - 1e-3f, out tx, out ty);
					if (!game.IsTileMoveable(tx - 1, ty, 2))
					{
						game.GetTileRect(tx, ty, out tx0, out ty0, out tx1, out ty1);
						moveX = Mathf.Max(moveX, tx0 - x0);
					}
				}
				else 
				{
					game.GetTileCoordFromPosition(x1 - 1e-3f, y0 + 1e-3f, out tx, out ty);
					if (game.IsTileOutOfBound(tx + 1, ty) && nextMapDir == -1) nextMapDir = 0;

					if (!game.IsTileMoveable(tx + 1, ty, 0))
					{
						game.GetTileRect(tx, ty, out tx0, out ty0, out tx1, out ty1);
						moveX = Mathf.Min(moveX, tx1 - x1);
					}

					game.GetTileCoordFromPosition(x1 - 1e-3f, y1 - 1e-3f, out tx, out ty);
					if (!game.IsTileMoveable(tx + 1, ty, 0))
					{
						game.GetTileRect(tx, ty, out tx0, out ty0, out tx1, out ty1);
						moveX = Mathf.Min(moveX, tx1 - x1);
					}
				}
			}

			if (dy != 0)
			{
				if (dy < 0)
				{
					game.GetTileCoordFromPosition(x0 + 1e-3f, y0 + 1e-3f, out tx, out ty);
					if (game.IsTileOutOfBound(tx, ty + 1) && nextMapDir == -1) nextMapDir = 1;

					if (!game.IsTileMoveable(tx, ty + 1, 1))
					{
						game.GetTileRect(tx, ty, out tx0, out ty0, out tx1, out ty1);
						moveY = Mathf.Max(moveY, ty0 - y0);
					}

					game.GetTileCoordFromPosition(x1 - 1e-3f, y0 + 1e-3f, out tx, out ty);
					if (!game.IsTileMoveable(tx, ty + 1, 1))
					{
						game.GetTileRect(tx, ty, out tx0, out ty0, out tx1, out ty1);
						moveY = Mathf.Max(moveY, ty0 - y0);
					}
				}
				else 
				{
					game.GetTileCoordFromPosition(x0 + 1e-3f, y1 - 1e-3f, out tx, out ty);
					if (game.IsTileOutOfBound(tx, ty - 1) && nextMapDir == -1) nextMapDir = 3;

					if (!game.IsTileMoveable(tx, ty - 1, 3))
					{
						game.GetTileRect(tx, ty, out tx0, out ty0, out tx1, out ty1);
						moveY = Mathf.Min(moveY, ty1 - y1);
					}

					game.GetTileCoordFromPosition(x1 - 1e-3f, y1 - 1e-3f, out tx, out ty);
					if (!game.IsTileMoveable(tx, ty - 1, 3))
					{
						game.GetTileRect(tx, ty, out tx0, out ty0, out tx1, out ty1);
						moveY = Mathf.Min(moveY, ty1 - y1);
					}
				}
			}
			
			x += moveX;
			y += moveY;

			CalculateBound();
			UpdatePosition();
		}
		else 
		{
			isWalking = false;
		}

		int lastAnimationId = curAnimationId;
		curAnimationId = (dir * 2) + (isWalking ? 1 : 0);

		if (curAnimationId != lastAnimationId)
		{
			animator.Play((isWalking ? "walk" : "idle") + "_" + directionNames[dir]);
		}		
	}
}

