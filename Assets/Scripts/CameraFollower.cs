using UnityEngine;
using System.Collections;

public class CameraFollower : MonoBehaviour
{
	[HideInInspector]
	public Transform obj;

	[HideInInspector]
	public float minX;

	[HideInInspector]
	public float minY;

	[HideInInspector]
	public float maxX;

	[HideInInspector]
	public float maxY;

	private Vector3 velocity = Vector3.zero;

	// Use this for initialization
	void Start ()
	{
	}
	
	// Update is called once per frame
	void Update()
	{
		if (obj != null)
		{
			Vector3 pos = obj.position;
			pos.z = -10.0f;

			pos.x = Mathf.Clamp(pos.x, minX, maxX);
			pos.y = Mathf.Clamp(pos.y, minY, maxY);

			transform.position = Vector3.SmoothDamp(transform.position, pos, ref velocity, 0.3f);
			//transform.position = pos;
		}
	}

	public void ForcePosition()
	{
		if (obj != null)
		{
			Vector3 pos = obj.position;
			pos.z = -10.0f;
			
			pos.x = Mathf.Clamp(pos.x, minX, maxX);
			pos.y = Mathf.Clamp(pos.y, minY, maxY);
			
			transform.position = pos;
			velocity = Vector3.zero;
		}
	}
}

