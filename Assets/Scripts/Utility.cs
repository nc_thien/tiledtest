using System.IO;
using UnityEngine;

public static class Utility
{
	public static Texture2D GetTexture(string path)
	{
		return Resources.Load<Texture2D>(path);
	}

	public static FileStream GetFileStream(string path)
	{
		string fullPath = Application.persistentDataPath + "/" + path; 
		if (!File.Exists(fullPath)) UnpackFile(path);

		if (File.Exists(fullPath))
		{
			FileStream file = File.Open(fullPath, FileMode.Open);    
			return file;
		}

		return null;
	}

	private static void UnpackFile(string path)
	{
		string destinationPath = System.IO.Path.Combine(Application.persistentDataPath, path);
		string sourcePath = System.IO.Path.Combine(Application.streamingAssetsPath, path);
		
		if (!System.IO.File.Exists(destinationPath) || (System.IO.File.GetLastWriteTimeUtc(sourcePath) > System.IO.File.GetLastWriteTimeUtc(destinationPath))) 
		{            
			System.IO.FileInfo file = new System.IO.FileInfo(destinationPath);
			file.Directory.Create(); 

			if (sourcePath.Contains ("://")) // Android  
			{
				WWW www = new WWW (sourcePath);
				while (!www.isDone) {;}               

				if (string.IsNullOrEmpty(www.error)) 
				{                  
					System.IO.File.WriteAllBytes(destinationPath, www.bytes);
				} 
				else 
				{
					Debug.Log(path + " doesn't exist in the StreamingAssets Folder");
				}                   
			} 
			else // Mac, Windows, Iphone                
			{
				if (System.IO.File.Exists(sourcePath)) 
				{                    
					//copy file - alle systems except Android
					System.IO.File.Copy(sourcePath, destinationPath, true);                    
				} 
				else 
				{
					Debug.Log(path + " doesn't exist in the StreamingAssets Folder");
				}                   
			}
		}
	}
}